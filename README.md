# Backend Alkemy Blog

## Dependencies

* express
* sequelize
* mysql2

## Start project
```
npm start

```

### Elements of a post 

* id
* title
* body
* category
* image
* date