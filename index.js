const express = require('express')
const postRouter = require('./api/posts/postRouter')

const app = express()

app.get("/", (req, res) => {
    res.send("go to /posts to see the list of posts")
})

app.use('/posts', postRouter)

app.listen(5000, () => console.log("listening on port 5000"))