const express = require('express')
var { posts } = require('../../database.models');

const postRouter = express.Router()

postRouter.get("/", (req, res) => {
    posts.findAll()
        .then(posts => {
            //res.send(posts)
            res.json({
                data: posts,
            });

        })
        .catch(error => {
            console.log(error);
        })
})

postRouter.get("/:id", (req, res) => {
    posts.findByPk(req.params.id)
        .then((post) => res.json(post))
})

postRouter.post("/", (req, res) => {
    let newPost = req.body

    if (!newPost.title || !newPost.body || !newPost.category) {
        res.status(400).send("missing data")
        return
    }

    posts.create(newPost)
        .then(newPost => {
            return res.redirect('/posts/' + newPost.id)
        })

})

postRouter.put('/:id', (req, res) => {
    let updatedPost = req.body

    posts.update(updatedPost,
        {
            where: {
                id: req.params.id
            }
        }).then((result) => res.redirect('/posts/' + req.params.id))
})

postRouter.delete("/:id", (req, res) => {
    posts.destroy(
        {
            where: {
                id: req.params.id
            }
        }).then((result) => res.redirect('/posts/'))
})

module.exports = postRouter;
